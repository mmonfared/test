from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
# from selenium.webdriver import Remote

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--headless")
# service = Service(executable_path=ChromeDriverManager().install())
# driver = webdriver.Chrome(service=service, options=chrome_options)
driver = webdriver.Remote(command_executor='http://selenium__standalone-chrome:4444/wd/hub', options=chrome_options)
driver.get("http://google.com")
driver.find_element('name', 'q').send_keys("Wikipedia")
print("Success!")

